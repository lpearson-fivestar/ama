### IaC AMA DEMO CODE
---

This code is what will be used throughout the IaC demostration during the AMA on 2019/02/07.

The majority of it is written in [Terraform](https://www.terraform.io) which has [docs](https://www.terraform.io/docs/index.html) and can be [downloaded](https://www.terraform.io/downloads.html).

Specifically we will be working with the providers with [AWS](https://www.terraform.io/docs/providers/aws/index.html) and [vSphere](https://www.terraform.io/docs/providers/vsphere/index.html).  Check out the rest of the providers that terraform has [here](https://www.terraform.io/docs/providers/index.html).

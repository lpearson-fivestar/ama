provider "aws" {
  region  = "us-west-2"
  version = "~> 1.57"
}

provider "null" {
  version = "~> 2.0"
}

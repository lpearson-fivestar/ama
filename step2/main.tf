#### VSPHERE ####
data "vsphere_datacenter" "dc" {
  name = "datacenter"
}

resource "vsphere_file" "ubuntu_disk_upload" {
  datacenter       = "${data.vsphere_datacenter.dc.name}"
  datastore        = "datastore1"
  source_file      = "../isos/ubuntu-1804-server.iso"
  destination_file = "/isos/ubuntu-1804-server.iso"
}

variable "hosts" {
  default = [
    "192.168.1.120",
  ]
}

variable "network_interfaces" {
  default = [
    "vmnic0"
  ]
}

data "vsphere_host" "hosts" {
  count         = "${length(var.hosts)}"
  name          = "${var.hosts[count.index]}"
  datacenter_id = "${data.vsphere_datacenter.dc.id}"
}

resource "vsphere_compute_cluster" "cluster" {
  name            = "cluster"
  datacenter_id   = "${data.vsphere_datacenter.dc.id}"
  host_system_ids = ["${data.vsphere_host.hosts.*.id}"]

  drs_enabled = false

  ha_enabled = false
}

data "vsphere_datastore" "datastore" {
  name          = "datastore1"
  datacenter_id = "${data.vsphere_datacenter.dc.id}"
}

data "vsphere_network" "network" {
  name          = "VM Network"
  datacenter_id = "${data.vsphere_datacenter.dc.id}"
}

resource "vsphere_virtual_machine" "vm" {
  name             = "terraform-test"
  resource_pool_id = "${vsphere_compute_cluster.cluster.resource_pool_id}"
  datastore_id     = "${data.vsphere_datastore.datastore.id}"

  num_cpus = 2
  memory   = 2048
  guest_id = "ubuntu64Guest"

  network_interface {
    network_id = "${data.vsphere_network.network.id}"
  }

  disk {
    label = "disk0"
    size  = 20
  }

  cdrom {
    datastore_id = "${data.vsphere_datastore.datastore.id}"
    path         = "isos/ubuntu-1804-server.iso"
  }

  depends_on = [
      "vsphere_file.ubuntu_disk_upload"
  ]
}

# This should only be here if the host was not original setup at all
# resource "vsphere_host_virtual_switch" "vSwitch0" {
#   name           = "vSwitch0"
#   host_system_id = "${data.vsphere_host.hosts.0.id}"
#
#   network_adapters = ["${var.network_interfaces}"]
#
#   active_nics  = ["${var.network_interfaces}"]
#   standby_nics = ["${var.network_interfaces}"]
# }

# resource "vsphere_host_virtual_switch" "switch1" {
#   name           = "vSwitch1"
#   host_system_id = "${data.vsphere_host.hosts.0.id}"
#
#   number_of_ports = "504"
# }

# resource "vsphere_host_virtual_switch" "switch2" {
#   name           = "vSwitch2"
#   host_system_id = "${data.vsphere_host.hosts.0.id}"
#
#   network_adapters = ["${var.network_interfaces}"]
#
#   active_nics  = ["${var.network_interfaces}"]
#   standby_nics = ["${var.network_interfaces}"]
#
#
#   number_of_ports = "256"
# }

#### END VSPHERE ####



### AWS ###
resource "aws_route53_zone" "primary" {
  name = "ama.pearsonprogramming.com"
}

module "ama1" {
  source = "terraform-aws-modules/vpc/aws"

  name = "fs-usw2-ama-1"
  cidr = "10.10.0.0/16"

  azs             = ["us-west-2a", "us-west-2b"]
  private_subnets = ["10.10.110.0/24", "10.10.120.0/24"]
  public_subnets  = ["10.10.10.0/24", "10.10.20.0/24"]

  enable_nat_gateway = "true"
  single_nat_gateway = "true"

  enable_dns_support   = "true"
  enable_dns_hostnames = "true"

  tags = {
    Terraform   = "true"
    Namespace   = "fs"
    Environment = "ama-1"
  }
}

resource "null_resource" "ama1_dependency" {
  depends_on = ["module.ama1"]
}

module "ama2" {
  source = "terraform-aws-modules/vpc/aws"

  name = "fs-usw2-ama-2"
  cidr = "10.20.0.0/16"

  azs             = ["us-west-2a", "us-west-2b"]
  private_subnets = ["10.20.110.0/24", "10.20.120.0/24"]
  public_subnets  = ["10.20.10.0/24", "10.20.20.0/24"]

  enable_nat_gateway = "true"
  single_nat_gateway = "true"

  enable_dns_support   = "true"
  enable_dns_hostnames = "true"

  tags = {
    Terraform   = "true"
    Namespace   = "fs"
    Environment = "ama-2"
  }
}

resource "null_resource" "ama2_dependency" {
  depends_on = ["module.ama2"]
}

resource "aws_vpc_peering_connection" "ama1_to_ama2" {
  vpc_id      = "${module.ama1.vpc_id}"
  peer_vpc_id = "${module.ama2.vpc_id}"
  auto_accept = true

  tags {
    Name = "VPC Peering between ama1 and ama2"
  }
}

resource "aws_route" "ama1_to_ama2" {
  route_table_id            = "${element(module.ama1.public_route_table_ids, 1)}"
  destination_cidr_block    = "${module.ama2.vpc_cidr_block}"
  vpc_peering_connection_id = "${aws_vpc_peering_connection.ama1_to_ama2.id}"
}

resource "aws_route" "ama2_to_ama1" {
  route_table_id            = "${element(module.ama2.private_route_table_ids, 1)}"
  destination_cidr_block    = "${module.ama1.vpc_cidr_block}"
  vpc_peering_connection_id = "${aws_vpc_peering_connection.ama1_to_ama2.id}"
}

resource "aws_security_group_rule" "allow_ssh_from_all" {
  type              = "ingress"
  from_port         = 22
  to_port           = 22
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = "${module.ama1.default_security_group_id}"
}

resource "aws_security_group_rule" "allow_ssh_from_ama1" {
  type              = "ingress"
  from_port         = 22
  to_port           = 22
  protocol          = "tcp"
  cidr_blocks       = ["${module.ama1.vpc_cidr_block}"]
  security_group_id = "${module.ama2.default_security_group_id}"
}

data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-bionic-18.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"] # Canonical
}

module "ama1-bastion" {
  source = "../modules/terraform-aws-ec2-bastion-server"

  namespace       = "fs"
  stage           = "ama1"
  name            = "bastion"
  ami             = "${data.aws_ami.ubuntu.id}"
  zone_id         = "${aws_route53_zone.primary.zone_id}"
  vpc_id          = "${module.ama1.vpc_id}"
  subnets         = ["${module.ama1.public_subnets}"]
  key_name        = "ama"
  ssh_user        = "ubuntu"
  security_groups = ["${module.ama1.default_security_group_id}"]
}

module "ama2-bastion" {
  source = "../modules/terraform-aws-ec2-bastion-server"

  namespace       = "fs"
  stage           = "ama2"
  name            = "bastion"
  ami             = "${data.aws_ami.ubuntu.id}"
  zone_id         = "${aws_route53_zone.primary.zone_id}"
  vpc_id          = "${module.ama2.vpc_id}"
  subnets         = ["${module.ama2.private_subnets}"]
  key_name        = "ama"
  ssh_user        = "ubuntu"
  security_groups = ["${module.ama2.default_security_group_id}"]
}

#### END AWS ####

resource "vsphere_folder" "desktops" {
  path          = "Desktops"
  type          = "vm"
  datacenter_id = "${data.vsphere_datacenter.dc.id}"
}

resource "vsphere_folder" "replicas" {
  path          = "Replicas"
  type          = "vm"
  datacenter_id = "${data.vsphere_datacenter.dc.id}"
}

resource "vsphere_folder" "servers" {
  path          = "Servers"
  type          = "vm"
  datacenter_id = "${data.vsphere_datacenter.dc.id}"
}

resource "vsphere_folder" "dnb" {
  path          = "${vsphere_folder.servers.path}/Do Not Backup"
  type          = "vm"
  datacenter_id = "${data.vsphere_datacenter.dc.id}"
}

resource "vsphere_folder" "templates" {
  path          = "Templates"
  type          = "vm"
  datacenter_id = "${data.vsphere_datacenter.dc.id}"
}

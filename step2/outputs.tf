output "hosts" {
    value = "${data.vsphere_host.hosts.*.id}"
}

output "datacenter_name" {
  value = "${data.vsphere_datacenter.dc.name}"
}

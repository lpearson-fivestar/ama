provider "vsphere" {
  user           = "administrator@vsphere.local"
  password       = "Password1!"
  vsphere_server = "192.168.1.150"

  # If you have a self-signed cert
  allow_unverified_ssl = true
}
